angular.module('profiles.controllers', []);
angular.module('profiles.config', []);
angular.module('profiles.models', []);

angular.module('profiles', [
  'ionic',
  'ionic-material',
  'profiles.controllers',
  'profiles.config',
  'profiles.models',
  'satellizer'
]);

angular.module('profiles').constant('URL', 'http://flori.sevillalandia.es/api/');

angular.module('profiles').config(function($httpProvider, $urlRouterProvider, $authProvider, URL) {

  $authProvider.loginUrl = URL+'authenticate';
  $urlRouterProvider.otherwise('/app/login');

  //- Login Facebook
  $authProvider.facebook({
    clientId: '703123223178686'
  });

  
});

angular.module('profiles').run(function($ionicPlatform , $rootScope, $timeout) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

     $rootScope.authStatus = false;
	 //stateChange event
	  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		  $rootScope.authStatus = toState.authStatus;
		  if($rootScope.authStatus){
			  
			
		  }
    });

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		console.log("URL : "+toState.url);
		if(toState.url=='/dashboard'){
			console.log("match : "+toState.url);
			$timeout(function(){
				angular.element(document.querySelector('#leftMenu' )).removeClass("hide");
			},1000);
		}	
	});

});
