(function () {
	'use strict';

	angular.module(
		'profiles.config', ['ionic']
	);

	angular.module('profiles.config').config(profilesConfig);

	function profilesConfig( $urlRouterProvider, $stateProvider){

		$stateProvider
		  .state('app', {
		    url: '/app',
		    abstract: true,
		    templateUrl: 'modules/views/layouts/menu.html',
		    controller: 'AuthenticateController'
		  })

		//--------------------------------------

		 .state('app.login', {
		    url: '/login',
		    views: {
		      'menuContent': {
		        templateUrl: 'modules/views/authenticate/tab-signin.html'
		      }
		    },
			authStatus: false
		  })
		 .state('app.signup', {
		    url: '/signup',
		    views: {
		      'menuContent': {
		        templateUrl: 'modules/views/authenticate/tab-signup.html',
		      }
		   },
			authStatus: false
		  })
		//--------------------------------------


		  .state('app.dashboard', {
		    url: '/dashboard',
		    views: {
		      'menuContent': {
		        templateUrl: 'modules/views/layouts/dashboard.html',
				controller: 'DashboardController'
		      }
		     },
			 authStatus: true
		  })


		    .state('app.profiles', {
		      url: '/profiles',
		      views: {
		        'menuContent': {
		          templateUrl: 'modules/views/users/profiles.html',
		          controller: 'ProfilesController'
		        }
		      }
		    })

		  .state('app.profile', {
		    url: '/profile/:profileId',
		    views: {
		      'menuContent': {
		        templateUrl: 'modules/views/users/profile-detail.html',
		        controller: 'ProfileController'
		      }
		    }
		  });

	}

})();
