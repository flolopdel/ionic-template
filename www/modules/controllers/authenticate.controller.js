(function () {
	'use strict';

	angular.module('profiles.controllers').controller('AuthenticateController', authenticateController);

	function authenticateController(
		$ionicModal,
		$scope,
		$ionicPopover,
		$timeout,
		$location,
	  $auth,
    UserModel,
    utils)
	{

    $scope.login = login;
    $scope.logout = logout;
    $scope.register = register;
    $scope.authenticate = authenticate;

    function login(user) {
        $auth.login(user)
        .then(function() {
            $location.path('/app/dashboard');
        })
        .catch(utils.showError);

  	}

    function logout() {   

      $auth.logout();
      localStorage.clear();
      $location.path('/app/login');   
    }

    function register(user) {

        UserModel.createUser(user).then(function(response){
              $scope.login(user);
        })
        .catch(utils.showError);

    }

    function authenticate(provider) {
      $auth.authenticate(provider);
    };
	}

})();
