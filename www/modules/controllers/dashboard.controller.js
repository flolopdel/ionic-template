(function () {
	'use strict';

	angular.module('profiles.controllers').controller('DashboardController', dashboardController);

	function dashboardController(
		$scope,
		UserModel,
		utils)
	{

		UserModel.getUsers().then(function(response){
	          $scope.profiles 				= response.data.data;
		})
        .catch(utils.showError);

	}

})();
