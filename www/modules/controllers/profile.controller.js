(function () {
	'use strict';

	angular.module('profiles.controllers').controller('ProfileController', profileController);

	function profileController(
		$scope,
		$stateParams,
		UserModel, 
		utils)
	{
    
    	UserModel.getUser($stateParams.profileId).then(function(response){
	          $scope.profile 				= response.data.data;
		})
        .catch(utils.showError);

	}

})();
