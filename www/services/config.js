(function () {
	'use strict';

	angular.module('profiles').factory('config', config);

	function config(URL) {

		var config = {

			"app": {
				"version": "v1.0.0",
			},
			"debugMode": true,
			"apiServer": {
				"baseUrl": URL,
				"authorization": "",
				"timeout": 20000
			},
			"visorServer": "http://myvaughan.pre.grupovaughan.net/#/developer/dynamics-viewer?url="+URL+"legoCaches/",
			"cap": {
				"timeout": 10000
			},
			"device": {
				"type": getDeviceType(), // desktop, mobile, web
				"so": getDeviceOS() // android, ios, macos, windows, linux
			},
			"user": {
				"defaultLanguage": "es-ES"
			},
			"languages": [
				{"id": "es-ES", "translationString": "language:es-ES"},
				{"id": "ca-ES", "translationString": "language:ca-ES"},
				{"id": "gl-ES", "translationString": "language:gl-ES"},
				{"id": "va-ES", "translationString": "language:va-ES"},
				{"id": "ba-ES", "translationString": "language:ba-ES"},
				{"id": "eu-ES", "translationString": "language:eu-ES"},
				{"id": "en-EN", "translationString": "language:en-EN"}
			]
		};

		function getDeviceType() {

			// Posible values: desktop, mobile, web

			if ((typeof(cordova) !== "undefined") && /^file:\/{3}[^\/]/i.test(window.location.href)) {
				return "mobile";
			} else if (typeof(process) !== "undefined") {
				return "desktop";
			} else {
				return "web";
			}

			return "unknown";
		}

		function getDeviceOS() {

			// Posible values: android, ios, macos, windows, linux

			if (navigator.userAgent.match(/Android/i) !== null) {
				return "android";
			} else if (navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)/i) !== null) {
				return "ios";
			} else if (navigator.appVersion.indexOf("Win") !== -1) {
				return "windows";
			} else if (navigator.appVersion.indexOf("Linux") !== -1) {
				return "linux";
			} else if (navigator.appVersion.indexOf("Mac") !== -1) {
				return "macos";
			}

			return "unknown";
		}

		return config;
	}

})();
