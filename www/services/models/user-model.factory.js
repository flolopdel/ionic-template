(function () {
    'use strict';

    angular.module('profiles.models').factory('UserModel', userModel);

    function userModel(utils){

    	function getUser(id, params){
            params = params || {};
            return utils.httpRequest( "GET", "users/"+id, params );
        }

        function createUser(params){
            params = params || {};
            return utils.httpRequest( "POST", "register", params );
        }

        function getUsers(params){
            params = params || {};
            return utils.httpRequest( "GET", "users", params );
        }
        
        return {

            getUser:getUser,
            createUser:createUser,  
            getUsers:getUsers  
        };
        	
    }

})();
