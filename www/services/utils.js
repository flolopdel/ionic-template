(function () {
	'use strict';

	angular.module('profiles').factory('utils', utils);

	function utils(
		$q,
		$http,
		$window,
		$ionicPopup,
		config,
		$state,
		$auth) {

			var utils = {
				'httpRequest': httpRequest,
				'openExternalUrl': openExternalUrl,
				'logoutSession':logoutSession,
				'showError':showError,
				'showAlert':showAlert
			};

			function httpRequest( method, endpoint, data ) {

				return $http({
					method: method,
					url: config.apiServer.baseUrl + endpoint,
					headers: {
						'Authorization': config.apiServer.authorization
					},
					data: (method !== "GET")?data:{},
					params: (method === "GET")?data:{},
					timeout: config.apiServer.timeout
				});
			}

			function openExternalUrl(url){

				if (config.device.type === "desktop") {

					var gui = require('nw.gui');
					gui.Shell.openExternal(url);

				} else if(config.device.type === "mobile") {

					var options = {
						location: 'yes',
						clearcache: 'yes',
						toolbar: 'no'
					};

				} else {

					window.open(url, "_blank");

				}
			}

			function logoutSession(response){
				$state.go('login');
				$auth.logout();
				localStorage.clear();

			}

			function showError(error) {
	          $ionicPopup.alert({
	            title: 'Error',
	            content: error.message || (error.data && error.data.message) || error
	          });
	        }
	        
		  	function showAlert(title, msg) {
		  	   $ionicPopup.alert({
		  		 title: title,
		  		 template: msg
		  	   });
		  	}

			return utils;
		}

	})();
